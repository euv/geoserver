# Base image
FROM ubuntu:22.04 AS build
#FROM debian:buster AS base
ARG DEBIAN_FRONTEND=noninteractive

ARG TZ=Europe/Berlin
ARG RETRIES=5

ARG GEOSERVER_MAJOR=2
ARG GEOSERVER_MINOR=26
ARG GEOSERVER_BUILD=2
ARG GEOSERVER_VERSION=${GEOSERVER_MAJOR}.${GEOSERVER_MINOR}.${GEOSERVER_BUILD}
ARG GEOSERVER_COMMUNITY=${GEOSERVER_MAJOR}.${GEOSERVER_MINOR}
ARG JPEGTURBO_VERSION=2.1.5.1
ARG MIRROR=netcologne

RUN apt-get -y update && \
  apt-get -y dist-upgrade && \
  apt-get -y install curl unzip && \
  rm -rf /var/lib/apt/lists/*

RUN curl --retry $RETRIES -L -o geoserver.zip https://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/geoserver-${GEOSERVER_VERSION}-bin.zip/download?use_mirror=${MIRROR}

RUN curl --retry $RETRIES -L -o geoserver-app-schema-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-app-schema-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-charts-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-charts-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-querylayer-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-querylayer-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-css-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-css-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-cas-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-cas-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-inspire-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-inspire-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-printing-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-printing-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-ysld-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-ysld-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-mbstyle-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-mbstyle-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-web-resource-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-web-resource-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-importer-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-importer-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-importer-bdb-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-importer-bdb-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-libjpeg-turbo-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-libjpeg-turbo-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-grib-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-grib-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-csw-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-csw-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-wcs2_0-eo-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-wcs2_0-eo-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-wps-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-wps-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-pyramid-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-pyramid-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-vectortiles-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-vectortiles-plugin.zip/download?use_mirror=${MIRROR}
#RUN curl --retry $RETRIES -L -o geoserver-wps-cluster-hazelcast-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-wps-cluster-hazelcast-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-sldservice-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-sldservice-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-mongodb-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-mongodb-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-control-flow-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-control-flow-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-imagemosaic-jdbc-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-imagemosaic-jdbc-plugin.zip/download?use_mirror=${MIRROR}
#RUN curl --retry $RETRIES -L -o geoserver-gdal-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-gdal-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-mbtiles-store-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-mbtiles-store-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-mbtiles-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-mbtiles-plugin.zip/download?use_mirror=${MIRROR}
RUN curl --retry $RETRIES -L -o geoserver-monitor-plugin.zip -O http://sourceforge.net/projects/geoserver/files/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-monitor-plugin.zip/download?use_mirror=${MIRROR}
#RUN curl --retry $RETRIES -L -o geoserver-activeMQ-broker-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/community-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-activeMQ-broker-plugin.zip
#RUN curl --retry $RETRIES -L -o geoserver-jms-cluster-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/community-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-jms-cluster-plugin.zip
#RUN curl --retry $RETRIES -L -o geoserver-authkey-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/ext-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-authkey-plugin.zip
#RUN curl --retry $RETRIES -L -o geoserver-sec-keycloak-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/community-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-sec-keycloak-plugin.zip
RUN curl --retry $RETRIES -L -o geoserver-sec-oauth2-openid-connect-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/community-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-sec-oauth2-openid-connect-plugin.zip
#RUN curl --retry $RETRIES -L -o geoserver-sec-oauth2-openid-plugin.zip https://build.geoserver.org/geoserver/${GEOSERVER_COMMUNITY}.x/community-latest/geoserver-${GEOSERVER_COMMUNITY}-SNAPSHOT-sec-oauth2-openid-plugin.zip
#RUN curl --retry $RETRIES -L -o woodstox-core-6.4.0.jar https://repo1.maven.org/maven2/com/fasterxml/woodstox/woodstox-core/6.4.0/woodstox-core-6.4.0.jar
#RUN curl --retry $RETRIES -L -o stax2-api-4.2.1.jar https://repo1.maven.org/maven2/org/codehaus/woodstox/stax2-api/4.2.1/stax2-api-4.2.1.jar

RUN unzip -o geoserver.zip -d /opt/geoserver && \
  mkdir -p /geoserver/data.pre /geoserver/data && \
  cp -r /opt/geoserver/data_dir/* /geoserver/data.pre && \
  rm geoserver.zip

RUN for a in *.zip ; do unzip -n -d /opt/geoserver/webapps/geoserver/WEB-INF/lib $a ; done && \
  rm -rf /opt/geoserver/webapps/geoserver/WEB-INF/lib/tutorial/
  #&& \
  #cp woodstox-core-6.4.0.jar /opt/geoserver/webapps/geoserver/WEB-INF/lib/woodstox-core-6.4.0.jar && \
  #cp stax2-api-4.2.1.jar /opt/geoserver/webapps/geoserver/WEB-INF/lib/stax2-api-4.2.1.jar
 
RUN rm /opt/geoserver/webapps/geoserver/WEB-INF/lib/marlin-*.jar

RUN curl --retry $RETRIES -L -o libjpeg-turbo-official_${JPEGTURBO_VERSION}_amd64.deb https://sourceforge.net/projects/libjpeg-turbo/files/${JPEGTURBO_VERSION}/libjpeg-turbo-official_${JPEGTURBO_VERSION}_amd64.deb/download?use_mirror=${MIRROR} && \
    dpkg -i libjpeg-turbo-official_${JPEGTURBO_VERSION}_amd64.deb



# Release build
#FROM harbor.cluster.euvnt01.euv-stadtbetrieb.de/k8s/openjdk:11-jdk
#FROM openjdk:11-jdk-buster
FROM eclipse-temurin:17-jre-jammy
ARG DEBIAN_FRONTEND=noninteractive

LABEL MAINTAINER="Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>"

ENV JAVA_OPTS -Xmx1024m -Xms256m
#-Dsun.java2d.renderer=org.marlin.pisces.MarlinRenderingEngine
ENV GEOSERVER_DATA_DIR /geoserver/data
ENV GEOSERVER_HOME /opt/geoserver

#RUN apt-get -y update && \
#  apt-get dist-upgrade -y && \
#  echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && \
#  apt-get -y install libtcnative-1 ttf-bitstream-vera fontconfig gdal-bin libgdal-java && \
#  rm -rf /var/lib/apt/lists/*

RUN apt-get -y update && \
  apt-get dist-upgrade -y && \
  echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && \
  apt-get -y install libtcnative-1 ttf-bitstream-vera fontconfig libnetcdf19 && \
  rm -rf /var/lib/apt/lists/*

COPY --from=build /geoserver/ /geoserver/
COPY --from=build /opt/geoserver/ /opt/geoserver/
COPY --from=build /opt/libjpeg-turbo/ /opt/libjpeg-turbo/
COPY ./files/ /
RUN fc-cache -f -r

WORKDIR /opt/geoserver

VOLUME "/geoserver/data"

EXPOSE 8080

CMD [ "/entrypoint.sh" ]
