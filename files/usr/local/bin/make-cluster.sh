#!/bin/bash

if [ -z "${KUBERNETES_PORT+x}" ] ; then
    echo "Not running on Kubernetes"
else
    export CLUSTER_CONFIG_DIR=$GEOSERVER_DATA_DIR/cluster/$HOSTNAME
    mkdir -p $GEOSERVER_DATA_DIR/cluster/$HOSTNAME
    ./create-cluster-config.sh
fi
