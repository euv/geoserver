#!/bin/bash
ID=$(uuidgen)

cat <<+END
instanceName=$ID
readOnly=disabled
durable=true
brokerURL=$GEOSERVER_BROKER_URL
embeddedBroker=disabled
connection.retry=10
toggleMaster=true
topicName=VirtualTopic.geoserver
connection=enabled
toggleSlave=true
connection.maxwait=500
group=geoserver-cluster
+END
