#!/usr/bin/env bash

# Execute script in entrypoint
if [ -d /entrypoint.d ] ; then
    for f in /entrypoint.d/* ; do
        case "$f" in
            *.sh)
                if [ -x "$f" ] ; then
                    echo "$0: running $f"
                    "$f"
                else
                    echo "$0: sourcing $f"
                    . "$f"
                fi
        esac
        echo
    done
fi

if [ ! -f /geoserver/data/global.xml ] ; then
    echo Filling geoserver data path
    cp -frv /geoserver/data.pre/* /geoserver/data
fi

rm -f /opt/geoserver/webapps/geoserver/WEB-INF/lib/gdal-*.jar
if [ ! -f /opt/geoserver/webapps/geoserver/WEB-INF/lib/gdal.jar ] ; then
    echo Install GDAL library
    cp /usr/share/java/gdal.jar /opt/geoserver/webapps/geoserver/WEB-INF/lib/gdal.jar
fi

export JAVA_HOME="$(jrunscript -e 'java.lang.System.out.println(java.lang.System.getProperty("java.home"));')"
export JAVA_OPTS="${JAVA_OPTS} -Djava.library.path=/usr/lib/x86_64-linux-gnu/:/opt/libjpeg-turbo/lib64:/opt/jai-1_1_3/lib:/usr/lib:/usr/lib/jni:/usr/lib/x86_64-linux-gnu"
export LD_LIBRARY_PATH=/opt/libjpeg-turbo/lib64:/opt/jai-1_1_3/lib
chmod +x bin/*.sh

export JAVA_OPTS="${JAVA_OPTS} ${JAVA_GC}"

/opt/geoserver/bin/startup.sh
