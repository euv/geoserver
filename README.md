# ACHTUNG!
Im Geoserver Container 2.23.1 ist zur Zeit das GDAL Plugin ausgeschaltet!

Nach Anmerkung von Joel Joshua Laumann hab ich den Container von OpenJDK auf
Eclipse-Temurin 17-jre-jammy gewechselt! Danke für die Info!

LibJPEG Turbo ist wieder eingeschaltet. GDAL muss ich mit noch anschauen.

# Geoserver
Ein Docker Container für den Geoserver.org. Als Laufzeit Container wird ein Tomcat 9
eingesetzt.

Im Geoserver sind folgende Module mit eingebunden und können genutzt werden:

* App-Schema
* Charts
* Cross layer filter
* CSS Styling
* CAS
* Inspire
* Printing
* YSLD Styling
* libjpeg Turbo
* CSW
* WCS 2.0 EO
* WPS
* WPS hazelcast
* Image Pyramid
* Vector tiles
* SLDService
* MongoDB
* OAuth2 / Keycloak Authentication

## Build
Im Dockerfiler ist der default build so eingestellt das der Server unter der `URI /`
zu erreichen ist. Als build `ARG` kann der Key `WEBDIR` (default ist `ROOT`) auch 
auf eine unter URI gesetzt werden, z.B. `geoserver` was in `/geoserver` resultiert.

## Environment
Für den Container können folgenden ENVs gesetzt werden:

| Variable      | Verwendung                               |
| ------------- | ---------------------------------------- |
| JAVA_OPTS     | Einstellungen für die Java Runtime       |
| JAVA_GC       | Einstellungen für die Java Grabage collection. "-XX:+UseParNewGC" für Speicher kleiner 6GB und "–XX:+UseG1GC" für mehr als 6GB |

## Volumes
Im Container sind zwei Volumes vorhanden:

| Volume path          | Verwendung                                                     |
| -------------------- | -------------------------------------------------------------- |
| /geoserver/data      | Hier werden die Einstellungen des Geoservers gespeichert       |
| /geoserver/data/daten | Hier werden die Geodaten eingebunden z.B. Raster               |

## Betrieb
Der Geoserver kann als Deployment in einem K8S Cluster betrieben werden. Wenn Layers oder Einstellungen geändert werden muss
die Instanz Anzahl auf eins reduziert werden. Die Einstellungen für die anderen Instanzen werden nur beim Neustart geladen!
